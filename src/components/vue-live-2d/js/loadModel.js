window.LIVE2DCUBISMCORE = Live2DCubismCore
//如果资源在CDN，一定要写http://或者https://否则会以本域名请求且为相对路径
//Application全局变量
let app = null;
//模型渲染的位置
let tag_target;
//待机的动作索引
let idleIndex;
//登录的动作索引，只针对动作文件中有idel字段的
let loginIndex;
//回港动作，只针对碧蓝航线等有回港动作，动作文件中有home字段
let homeIndex;
//模型偏移位置
let model_x = 0;
let model_y = 0;
//渲染模型的宽高
let modelWidth;
let modelPath;
let modelHight;
//渲染模型的比例
let scale;
//测试用，加载时间起点，不保证准确性
let startTime = null;
//加载动作文件
function loadMotions(motions,key) {
    console.log(motions, 'motions');
    //动作总数
    let motionCount = 0;
    if (motions.length > 0) {
        for (let i = 0; i < motions.length; i++) {
                // console.log(modelPath.substr(0, modelPath.lastIndexOf('/') + 1) + motions[i].File,'12121');
                console.log(i);
            PIXI.loader.add('motion'+key + (motionCount + 1), modelPath.substr(0, modelPath.lastIndexOf('/') + 1) + motions[i].File, { xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.JSON });
            if (motions[i].File.indexOf('idle') != -1) {
                idleIndex = motionCount;
            } else if (motions[i].File.indexOf('login') != -1) {
                loginIndex = motionCount;
            } else if (motions[i].File.indexOf('home') != -1) {
                homeIndex = motionCount;
            }
            motionCount++;
        }
    } else {
        console.error('Not find motions');
    }
}

//另一种初始化模型方式
function initModel(data, path) {
        modelPath = path
    let model3Obj = { data: data, url: path.substr(0, path.lastIndexOf('/') + 1) };
    //清除loader内的内容，并清除缓存中的内容
    PIXI.loader.reset();
    PIXI.utils.destroyTextureCache();
    for (let key in data.FileReferences.Motions) {
        loadMotions(data.FileReferences.Motions[key],key);
    }
    //调用此方法直接加载，并传入设置模型的回调方法
    new LIVE2DCUBISMPIXI.ModelBuilder().buildFromModel3Json(
        PIXI.loader
            .on("start", loadStartHandler)
            .on("progress", loadProgressHandler)
            .on("complete", loadCompleteHandler),
        model3Obj,
        setModel
    );
}
//设置模型的回调方法
function setModel(model) {
    let canvas = document.querySelector(tag_target);
    let view = canvas
    //重复加载模型是，先停止渲染，否则后台WebGL会报警告
    if (app != null) { app.stop(); }
    app = new PIXI.Application(modelWidth, modelHight, { transparent: true, view: view });
    app.stage.addChild(model);
    app.stage.addChild(model.masks);
    let motions = setMotions(model, PIXI.loader.resources);
    setMouseTrick(model, app, canvas, motions);
    setOnResize(model, app);
}
//设置模型动作
function setMotions(model, resources) {
    //动作数组，存放格式化好的动作数据
    let motions = [];
    for (let key in resources) {
        if (key.indexOf('motion') != -1) {
            motions.push(LIVE2DCUBISMFRAMEWORK.Animation.fromMotion3Json(resources[key].data));
        }
    }
    let timeOut;
    if (motions.length > 0) {
        window.clearTimeout(timeOut);
        model.animator.addLayer("motion", LIVE2DCUBISMFRAMEWORK.BuiltinAnimationBlenders.OVERRIDE, 1.0);
        if (null != loginIndex && null != idleIndex) {//如果有登录和待机动作，则在登录动作完成后切换到待机动作
            model.animator.getLayer("motion").play(motions[loginIndex]);
            timeOut = setTimeout(function () { model.animator.getLayer("motion").play(motions[idleIndex]); }, motions[loginIndex].duration * 1000);
        } else {
            //如果没有登录动作，则默认播放第一个动作
            model.animator.getLayer("motion").play(motions[0]);
        }
    }
    return motions;
}
//设置鼠标追击
function setMouseTrick(model, app, canvas, motions) {
    let rect = canvas.getBoundingClientRect();
    let center_x = modelWidth / 2 + rect.left, center_y = modelHight / 2 + rect.top;
    let mouse_x = center_x, mouse_y = center_y;
    let angle_x = model.parameters.ids.indexOf("ParamAngleX");
    if (angle_x < 0) { angle_x = model.parameters.ids.indexOf("PARAM_ANGLE_X"); }
    let angle_y = model.parameters.ids.indexOf("ParamAngleY");
    if (angle_y < 0) { angle_y = model.parameters.ids.indexOf("PARAM_ANGLE_Y"); }
    let eye_x = model.parameters.ids.indexOf("ParamEyeBallX");
    if (eye_x < 0) { eye_x = model.parameters.ids.indexOf("PARAM_EYE_BALL_X"); }
    let eye_y = model.parameters.ids.indexOf("ParamEyeBallY");
    if (eye_y < 0) { eye_y = model.parameters.ids.indexOf("PARAM_EYE_BALL_Y"); }
    app.ticker.add(function (deltaTime) {
        rect = canvas.getBoundingClientRect();
        center_x = modelWidth / 2 + rect.left, center_y = modelHight / 2 + rect.top;
        let x = mouse_x - center_x;
        let y = mouse_y - center_y;
        model.parameters.values[angle_x] = x * 0.1;
        model.parameters.values[angle_y] = -y * 0.1;
        model.parameters.values[eye_x] = x * 0.005;
        model.parameters.values[eye_y] = -y * 0.005;
        model.update(deltaTime);
        model.masks.update(app.renderer);
    });
    let scrollElm = bodyOrHtml();
    let mouseMove;
    window.addEventListener("mousemove", function (e) {
        window.clearTimeout(mouseMove);
        mouse_x = e.pageX - scrollElm.scrollLeft;
        mouse_y = e.pageY - scrollElm.scrollTop;
        mouseMove = window.setTimeout(function () { mouse_x = center_x, mouse_y = center_y }, 5000);
    });
    let timeOut;
    window.addEventListener("click", function (e) {
        window.clearTimeout(timeOut);
        if (motions.length == 0) { return; }
        if (rect.left < mouse_x && mouse_x < (rect.left + rect.width) && rect.top < mouse_y && mouse_y < (rect.top + rect.height)) {
            let rand = Math.floor(Math.random() * motions.length);
            model.animator.getLayer("motion").stop();
            model.animator.getLayer("motion").play(motions[rand]);
            //如果有登录动作，则在随机播放动作结束后回到待机动作
            if (null != idleIndex) {
                timeOut = setTimeout(function () { model.animator.getLayer("motion").play(motions[idleIndex]); }, motions[rand].duration * 1000);
            }
        }
    });
    let onblur = false;
    let onfocusTime;
    sessionStorage.setItem('Onblur', '0');
    window.onblur = function (e) {
        if ('0' == sessionStorage.getItem('Onblur')) {
            onfocusTime = setTimeout(function () { sessionStorage.setItem('Onblur', '1'); }, 30000);
        }
    };
    window.onfocus = function (e) {
        window.clearTimeout(onfocusTime);
        if (motions.length > 0) {
            if ('1' == sessionStorage.getItem('Onblur')) {
                model.animator.getLayer("motion").stop();
                if (null != loginIndex && null != idleIndex) {//如果有回港和待机动作，则在登录动作完成后切换到待机动作
                    model.animator.getLayer("motion").play(motions[homeIndex]);
                    onfocusTime = setTimeout(function () { model.animator.getLayer("motion").play(motions[idleIndex]); sessionStorage.setItem('Onblur', '0'); }, motions[homeIndex].duration * 1000);
                } else {
                    //如果没有，则默认播放第一个动作
                    model.animator.getLayer("motion").play(motions[0]);
                }
            }
        }
    };
}
//设置浏览器onResize事件
function setOnResize(model, app) {
    let onResize = function (event) {
        if (event === void 0) { event = null; }
        let width = modelWidth;
        let height = modelHight;
        app.view.style.width = width + "px";
        app.view.style.height = height + "px";
        app.renderer.resize(width, height);
        model.position = new PIXI.Point(modelWidth / 2 + model_x, modelHight / 2 + model_y);
        model.scale = new PIXI.Point(scale, scale);
        model.masks.resize(app.view.width, app.view.height);
    };
    onResize();
    window.onresize = onResize;
}
//获取页面内容方法
function bodyOrHtml() {
    if ('scrollingElement' in document) { return document.scrollingElement; }
    if (navigator.userAgent.indexOf('WebKit') != -1) { return document.body; }
    return document.documentElement;
}
//加载模型开始时Handler
function loadStartHandler() {
    //优化加载开始计时
    startTime = new Date();
    console.log("Start loading Model at " + startTime);
}
//加载模型Handler，监控加载进度
function loadProgressHandler(loader) {
    console.log("progress: " + Math.round(loader.progress) + "%");
}
//加载模型结束Handler
function loadCompleteHandler() {
    let loadTime = new Date().getTime() - startTime.getTime();
    console.log('Model initialized in ' + loadTime / 1000 + ' second');
    PIXI.loader.off("start", loadStartHandler);//监听事件在加载完毕后取消
    PIXI.loader.off("progress", loadProgressHandler);//监听事件在加载完毕后取消
    PIXI.loader.off("complete", loadCompleteHandler);//监听事件在加载完毕后取消
}
//简单发送AJAX异步请求读取json文件
export default async function loadModel(target, modelPath, { width, height }) {
    modelWidth = width || 100
    modelHight = height || 100
    scale = Math.min(modelWidth, modelHight)
    tag_target = '#' + target
    const response = await fetch(modelPath).then(response => response.json())
     initModel(response, modelPath);
 //   initModelConfig(response, modelPath)
}




