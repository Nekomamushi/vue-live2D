import { loadLive2DCdn } from './js/utils'
import vueLive2d from './index.vue'


import * as PIXI from 'pixi.js'
window.PIXI = PIXI

const install = function (Vue, config = {}) {

  if (install.installed) return;
  install.installed = true;
  const defaultConfig = {
    cdn: 'https://cdn.jsdelivr.net/gh/Volcanione/live2Dmodel/',
    version: 2,
  }
  Object.keys(defaultConfig).forEach(key => {
    if (!config[key]) {
      config[key] = defaultConfig[key]
    }
  })
  const { version } = config
  //注册组件
  Vue.component('vue-live-2d', async function (resolve) {
    if (version === 2) {
      await loadLive2_0(config)
      Vue.prototype.$loadlive2d = window.loadlive2d
      Vue.prototype.$Live2D = window.Live2D
      Vue.prototype.$Live2DMotion = window.Live2DMotion
    } else if (version === 3) {
      await loadLive3_0(config)
      const { default: loadModel } = await import('./js/loadModel.js')
      Vue.prototype.$loadlive2d = loadModel
    }
    Vue.prototype._live2dConfig = {...config}
    console.log('初始化页面成功！');
    resolve(vueLive2d)
  })
}

//2初始化方法

async function loadLive2_0({ cdn, version }) {
  const path = cdn + version + '.0/js/live2d.min.js'
  await loadLive2DCdn(path)
}


//3初始化方法
async function loadLive3_0({ cdn, version }) {
  const pathArr = [
    `${cdn}${version}.0/js/core/live2dcubismcore.min.js`,
    `${cdn}${version}.0/js/framework/live2dcubismframework.js`,
    `${cdn}${version}.0/js/framework/live2dcubismpixi.js`,
  ]
  const pathCdn = pathArr.map(path => loadLive2DCdn(path))
  await Promise.all(pathCdn)
}

// 默认导出 install
export default {
  install,
};