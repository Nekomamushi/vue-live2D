import Vue from "vue";
import App from "./App.vue";
import "normalize.css";
import router from "./router";


import vueLive2d from '@/components/vue-live-2d/'
Vue.use(vueLive2d, {
  //cdn: 'https://cdn.jsdelivr.net/gh/Volcanione/live2Dmodel/',
  version: 3,
})


Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
